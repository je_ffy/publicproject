//
//  AppDelegate.h
//  JenkinsWithFastlane
//
//  Created by je_ffy on 2018/5/30.
//  Copyright © 2018年 je_ffy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

